package hr.fer.projekt.datasets;

import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;

public class SimpleGraphView {
	
	private static final String FILENAME1 = "D:\\Downloads\\facebook\\facebook\\0.edges";
	private static final String FILENAME2 = "D:\\Downloads\\facebook\\facebook\\348.edges";

   
	public Graph<Integer, String> g;
	public Graph<Integer, String> tempG;
	
	EdgesParser edgesParser = new EdgesParser();
    public SimpleGraphView() {
    	
    	g= new UndirectedSparseGraph<>();
    	tempG = new UndirectedSparseGraph<>();
    	
        edgesParser.parse(FILENAME1, g, 1);
        edgesParser.parse(FILENAME2, g, 1);		
       
    }
}

