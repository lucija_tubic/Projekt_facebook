package hr.fer.projekt.datasets;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import edu.uci.ics.jung.graph.Graph;

public class EdgesParser {	

	int i = 0;
	
	public void parse(String fileName, Graph<Integer, String> g, int k) {
		
		
		BufferedReader br = null;
		FileReader fr = null;

		try {

			fr = new FileReader(fileName);
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(fileName));

			while ((sCurrentLine = br.readLine()) != null) {
			       String[] splited = sCurrentLine.split("\\s+");
		    	   int node1 = Integer.parseInt(splited[0]);
		    	   int node2 = Integer.parseInt(splited[1]);
		    	   
		    	   i++;
		    	   String edgeName = Integer.toString(i*k);
		    	   g.addEdge(edgeName,node1,node2);
			}
			
		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (br != null)
					br.close();

				if (fr != null)
					fr.close();

			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}
		
		//System.out.println(nodes);
	}	
}