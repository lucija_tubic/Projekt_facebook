package hr.fer.projekt.datasets;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Paint;
import java.awt.Stroke;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import org.apache.commons.collections15.Transformer;
import edu.uci.ics.jung.algorithms.layout.KKLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import edu.uci.ics.jung.visualization.decorators.ToStringLabeller;
import edu.uci.ics.jung.visualization.renderers.Renderer.VertexLabel.Position;

public class GraphControl {

	public SimpleGraphView sgv;
	List<Integer> notToRemove = new ArrayList<>();
	public List<Integer> filteredVer = new ArrayList<>();

	public GraphControl() {
		sgv = new SimpleGraphView(); // This builds the graph
	}

	void draw(String less, String more) {

		// Transformer maps the vertex number to a vertex property
		Transformer<Integer, Paint> vertexColor = new Transformer<Integer, Paint>() {
			public Paint transform(Integer node) {
				if (filter(less, more).contains(node))
					return Color.GREEN;
				else
					return Color.RED;
			}
		};

		// Layout<V, E>, BasicVisualizationServer<V,E>
		Layout<Integer, String> layout1 = new KKLayout(sgv.tempG);
		layout1.setSize(new Dimension(1000, 1000));

		VisualizationViewer<Integer, String> vv = new VisualizationViewer<Integer, String>(layout1);
		vv.setPreferredSize(new Dimension(1000, 1000));

		// Setup up a new vertex to paint transformer...
		DefaultModalGraphMouse gm = new DefaultModalGraphMouse();
		gm.setMode(ModalGraphMouse.Mode.TRANSFORMING);
		vv.setGraphMouse(gm);

		// set color transformer and change color
		vv.getRenderContext().setVertexFillPaintTransformer(vertexColor);

		Integer[] allNodes = sgv.tempG.getVertices().toArray(new Integer[sgv.tempG.getVertexCount()]);
		for (int i : allNodes)
			vv.getRenderContext().getVertexFillPaintTransformer().transform(i);

		// Set up a new stroke Transformer for the edges
		float dash[] = { 10.0f };
		final Stroke edgeStroke = new BasicStroke(1.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash,
				0.0f);

		vv.getRenderContext().setVertexLabelTransformer(new ToStringLabeller());
		vv.getRenderer().getVertexLabelRenderer().setPosition(Position.CNTR);

		JFrame frame = new JFrame("Simple Graph View 2");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().add(vv);
		frame.pack();
		frame.setVisible(true);

	}

	public void resize(int graphSize) {

		float postotak = (float) graphSize / 100;
		int nodesToRemove = (int) Math.floor(sgv.g.getVertexCount() * (1 - postotak));

		for (Integer v : sgv.g.getVertices())
			sgv.tempG.addVertex(v);

		for (String e : sgv.g.getEdges())
			sgv.tempG.addEdge(e, sgv.g.getIncidentVertices(e));

		Integer[] allNodes = sgv.tempG.getVertices().toArray(new Integer[sgv.tempG.getVertexCount()]);
		for (int n = nodesToRemove; n > 0; n--) {
			sgv.tempG.removeVertex(allNodes[n]);
		}
		System.out.println("uklonjeni cvorovi= " + nodesToRemove + ", preostali cvorovi= " + sgv.tempG.getVertexCount()
				+ ", postotak= " + graphSize);
		System.out.println(sgv.g.getVertexCount());
	}

	public List<Integer> filter(String less, String more) {
		Collection<Integer> vertices = sgv.tempG.getVertices();
		List<Integer> ver = new ArrayList<>(vertices);
		List<Integer> filteredVer = new ArrayList<>();

		if (!more.equals("") && less.equals("")) {
			for (Integer v : ver) {
				if ((sgv.tempG.getNeighborCount(v) > Integer.parseInt(more))) {
					filteredVer.add(v);
				}
			}
		}

		if (more.equals("") && !less.equals("")) {
			for (Integer v : ver) {
				if (sgv.tempG.getNeighborCount(v) < Integer.parseInt(less)) {
					filteredVer.add(v);
				}
			}
		}

		if (!more.equals("") && !less.equals("")) {
			for (Integer v : ver) {
				if ((sgv.tempG.getNeighborCount(v) > Integer.parseInt(more))
						&& (sgv.tempG.getNeighborCount(v) < Integer.parseInt(less))) {
					filteredVer.add(v);
				}
			}
		}

		if (more.equals("") && less.equals("")) {
			for (Integer v : ver) {
				filteredVer.add(v);
			}
		}
		
		return filteredVer;
	}

	public boolean check(String less, String more, JFrame frame) {
		if (!less.equals("") & !more.equals("")) {
			if (Integer.parseInt(less) <= Integer.parseInt(more)) {
				JOptionPane.showMessageDialog(frame, "Wrong parameters!");
				return false;
			}
		}
		return true;
	}

}