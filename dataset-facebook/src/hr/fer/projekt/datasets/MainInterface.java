package hr.fer.projekt.datasets;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.EventQueue;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.awt.Font;
import java.awt.Color;

public class MainInterface {

	JFrame frame;
	public int sizeValue = 100;
	private JTextField tfMoreThen;
	private JTextField tfLessThen;
	private GraphControl graphControl = new GraphControl();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainInterface window = new MainInterface();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainInterface() {
		mainInterface();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void mainInterface() {

		frame = new JFrame();
		frame.setBounds(100, 100, 481, 461);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);

		JButton btnShowGraph = new JButton("Show graph");
		btnShowGraph.setFont(new Font("Tahoma", Font.BOLD, 13));

		btnShowGraph.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphControl.resize(sizeValue);
				if (graphControl.check(tfLessThen.getText(), tfMoreThen.getText(), frame))
					graphControl.draw(tfLessThen.getText(), tfMoreThen.getText());
			}

		});
		btnShowGraph.setBounds(12, 338, 133, 36);
		frame.getContentPane().add(btnShowGraph);

		JButton btnQuit = new JButton("Quit");
		btnQuit.setFont(new Font("Tahoma", Font.BOLD, 13));
		btnQuit.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				System.exit(0);

			}
		});
		btnQuit.setBounds(354, 338, 97, 36);
		frame.getContentPane().add(btnQuit);

		JLabel percentagesLable = new JLabel("100 %");
		percentagesLable.setBounds(387, 110, 64, 36);
		frame.getContentPane().add(percentagesLable);

		JSlider slider = new JSlider();
		slider.setValue(100);
		slider.setBounds(131, 110, 212, 36);
		frame.getContentPane().add(slider);
		slider.addChangeListener(new ChangeListener() {

			@Override
			public void stateChanged(ChangeEvent arg0) {
				percentagesLable.setText(slider.getValue() + " %");
				sizeValue = slider.getValue();

			}
		});

		JLabel lblDataSize = new JLabel("Data size");
		lblDataSize.setBounds(31, 110, 56, 36);
		frame.getContentPane().add(lblDataSize);

		JLabel lblShowOnlyNodes = new JLabel("Show nodes with a number of friends ");
		lblShowOnlyNodes.setBounds(31, 170, 283, 16);
		frame.getContentPane().add(lblShowOnlyNodes);

		JLabel lblMoreThen = new JLabel("more then:");
		lblMoreThen.setBounds(150, 214, 73, 16);
		frame.getContentPane().add(lblMoreThen);

		tfMoreThen = new JTextField();
		tfMoreThen.setColumns(10);
		tfMoreThen.setBounds(241, 211, 73, 22);
		frame.getContentPane().add(tfMoreThen);

		tfLessThen = new JTextField();
		tfLessThen.setColumns(10);
		tfLessThen.setBounds(241, 251, 73, 22);
		frame.getContentPane().add(tfLessThen);

		JLabel lblLessThen = new JLabel("less then:");
		lblLessThen.setBounds(150, 254, 73, 16);
		frame.getContentPane().add(lblLessThen);

		JButton btnList = new JButton("Friends finder");
		btnList.setFont(new Font("Tahoma", Font.BOLD, 13));

		btnList.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				graphControl.resize(sizeValue);
				if (graphControl.check(tfLessThen.getText(), tfMoreThen.getText(), frame))
					friendsList();
			}
		});
		btnList.setBounds(182, 338, 133, 36);
		frame.getContentPane().add(btnList);

		JLabel lblNewLabel = new JLabel("Data visualizer");
		lblNewLabel.setForeground(Color.RED);
		lblNewLabel.setFont(new Font("Courier New", Font.BOLD, 29));
		lblNewLabel.setBounds(103, 13, 276, 47);
		frame.getContentPane().add(lblNewLabel);

	}

	private void friendsList() {

		JFrame frame = new JFrame("Selecting JList");
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Container contentPane = frame.getContentPane();

		graphControl.filter(tfLessThen.getText(), tfMoreThen.getText());

		Integer[] verticesListFiltered = graphControl.filter(tfLessThen.getText(), tfMoreThen.getText())
				.toArray(new Integer[graphControl.filter(tfLessThen.getText(), tfMoreThen.getText()).size()]);

		JList jlist = new JList(verticesListFiltered);

		JScrollPane scrollPane1 = new JScrollPane(jlist);
		contentPane.add(scrollPane1, BorderLayout.WEST);

		JTextPane textPane = new JTextPane();
		textPane.setEditable(false);
		frame.getContentPane().add(textPane, BorderLayout.CENTER);

		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent listSelectionEvent) {
				List<Integer> friends = new ArrayList<>(
						graphControl.sgv.tempG.getNeighbors((Integer) jlist.getSelectedValue()));
				StringBuilder sb = new StringBuilder();
				for (Integer friend : friends) {
					sb.append(friend + "   ");
				}
				textPane.setText(sb.toString());
			}

		};
		jlist.addListSelectionListener(listSelectionListener);

		frame.setSize(350, 200);
		frame.setVisible(true);

	}
}